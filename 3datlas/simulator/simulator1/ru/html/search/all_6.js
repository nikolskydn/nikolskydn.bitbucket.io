var searchData=
[
  ['g_5fk_5f',['g_K_',['../classHodginHuxleyNeursCPU.html#a4a3ea8022f66fb77efb7ba485398607f',1,'HodginHuxleyNeursCPU']]],
  ['g_5fl_5f',['g_L_',['../classHodginHuxleyNeursCPU.html#afe7308fd4120986aaea685ebbc0c2bbe',1,'HodginHuxleyNeursCPU']]],
  ['g_5fna_5f',['g_Na_',['../classHodginHuxleyNeursCPU.html#a67c53739571b26dae49603580f151e00',1,'HodginHuxleyNeursCPU']]],
  ['genrandcompletegraphconns',['genRandCompleteGraphConns',['../group__Connects.html#gafb193bcaba61efba39c5d477a4f3f3ad',1,'genRandCompleteGraphConns(std::vector&lt; int &gt; &amp;preNeurs, std::vector&lt; int &gt; &amp;postNeurs, std::vector&lt; float &gt; &amp;weights, const int &amp;nNeurs, const int &amp;nNeursExc, const float &amp;weightMin, const float &amp;weightMax):&#160;tools_conns.cpp'],['../group__Connects.html#gafb193bcaba61efba39c5d477a4f3f3ad',1,'genRandCompleteGraphConns(std::vector&lt; int &gt; &amp;preNeurs, std::vector&lt; int &gt; &amp;postNeurs, std::vector&lt; float &gt; &amp;weights, const int &amp;nNeurs, const int &amp;nNeursExc, const float &amp;weightMin=50., const float &amp;weightMax=100.):&#160;tools_conns.cpp']]],
  ['getcurrents',['getCurrents',['../classConns.html#a2b09ad974b5730b95c6af6749fb9b55f',1,'Conns::getCurrents()'],['../group__Connects.html#ga1ad46f3d3e864e3e0d6397500f2e4ece',1,'ConnsCPU::getCurrents()'],['../classConnsGPU.html#a4bf450eff090869eab3d69c1edf78624',1,'ConnsGPU::getCurrents()']]],
  ['getnconns',['getNConns',['../classConns.html#a5ed006a54088639229c0b3ea44f57228',1,'Conns']]],
  ['getnneurs',['getNNeurs',['../classNeurs.html#a05ad11916699a0b3f0621e05075f80c1',1,'Neurs']]],
  ['getpotentials',['getPotentials',['../classNeurs.html#ae78673f3db3a1e899e239a6531385f5a',1,'Neurs::getPotentials()'],['../classNeursCPU.html#aba6f3d380725db62a451e5caa2391808',1,'NeursCPU::getPotentials()'],['../classNeursGPU.html#aca8f1b31bfac39f12fff0ff6c82d5985',1,'NeursGPU::getPotentials()']]],
  ['getspikes',['getSpikes',['../classNeurs.html#a4f001a095e19e0488b2e3973a164b457',1,'Neurs::getSpikes()'],['../classNeursCPU.html#a0a51059eae4aa91ca0ed00adcc45c94b',1,'NeursCPU::getSpikes()'],['../classNeursGPU.html#abf69e6be49d109e191e3f9d652775e13',1,'NeursGPU::getSpikes()']]],
  ['gettime',['getTime',['../classNeurs.html#a86d11f6abeb5cec0037f7f7112c5ffc1',1,'Neurs::getTime()'],['../classConns.html#a5c5fde130317e2dc81814fe7a641d2e3',1,'Conns::getTime()'],['../classAstrs.html#a7c007439464e629a55236f2ecbb0ea0c',1,'Astrs::getTime()']]]
];
