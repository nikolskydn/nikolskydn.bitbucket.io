var searchData=
[
  ['f_5fhodgin_5fhuxley_5feuler',['F_Hodgin_Huxley_Euler',['../model__hodgin__huxley__neurs__GPU_8cu.html#ad1c02dd91ff9e7acfb7bbe87e9854a36',1,'model_hodgin_huxley_neurs_GPU.cu']]],
  ['f_5fishikevich_5fkernel_5feuler',['F_Ishikevich_Kernel_Euler',['../model__izhikevich__neurs__GPU_8cu.html#a5c756699caa480d942b0e39afaaf01a0',1,'model_izhikevich_neurs_GPU.cu']]],
  ['f_5fishikevich_5fkernel_5frunge_5fkutta',['F_Ishikevich_Kernel_Runge_Kutta',['../model__izhikevich__neurs__GPU_8cu.html#ad5c13ecb4368beaa29f2cff660b50e14',1,'model_izhikevich_neurs_GPU.cu']]],
  ['function_5fhodgin_5fhuxley_5frunge_5fkutta',['Function_Hodgin_Huxley_Runge_Kutta',['../model__hodgin__huxley__neurs__GPU_8cu.html#a86e6cedc7c08da1c8b9e5390b76b7d19',1,'model_hodgin_huxley_neurs_GPU.cu']]]
];
