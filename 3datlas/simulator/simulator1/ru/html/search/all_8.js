var searchData=
[
  ['i_5fdevice_5f',['I_device_',['../classNeursGPU.html#ab8dee48372282a3ec29b3db6889c3bb8',1,'NeursGPU']]],
  ['i_5fhost_5f',['I_host_',['../classNeursGPU.html#ab5d4e30b105733db378c4513d7777ee1',1,'NeursGPU']]],
  ['ibegin_5f',['IBegin_',['../classNeurs.html#a74e675fa8fed31fc8fa0d0dc2d9ee896',1,'Neurs']]],
  ['iend_5f',['IEnd_',['../classNeurs.html#aa2c3c4b1a9977b20b804d85d10efe03e',1,'Neurs']]],
  ['initauxparams',['initAuxParams',['../classAstrocyte.html#a5c080d335e52cf476259e43256635377',1,'Astrocyte::initAuxParams()'],['../classHodginHuxleyNeurs.html#a3b1ff17c64cc832c7a13d7e8d592726d',1,'HodginHuxleyNeurs::initAuxParams()'],['../classHodginHuxleyNeursCPU.html#aa1d574f99ec0dd9360077c0e45f69a2c',1,'HodginHuxleyNeursCPU::initAuxParams()'],['../classIzhikevichNeurs.html#a13bd9dc9c3449da1f303787a64f5d97b',1,'IzhikevichNeurs::initAuxParams()'],['../classIzhikevichNeursCPU.html#ae0a9f7cca9d07710eba3d3ecf830386d',1,'IzhikevichNeursCPU::initAuxParams()'],['../classAstrocyte.html#a5c080d335e52cf476259e43256635377',1,'Astrocyte::initAuxParams()']]],
  ['initialization_2ecpp',['initialization.cpp',['../initialization_8cpp.html',1,'']]],
  ['initialization_2eh',['initialization.h',['../initialization_8h.html',1,'']]],
  ['initializeglobalvalues',['initializeGlobalValues',['../classInitializer.html#a04cd7cf7670e3cf4eed68534b13a597b',1,'Initializer']]],
  ['initializer',['Initializer',['../classInitializer.html',1,'Initializer'],['../classInitializer.html#a3a520b930db5eada7e05982b550c8128',1,'Initializer::Initializer(const std::string &amp;filename)'],['../classInitializer.html#a419654c9a02e79f97caba7fa38c8a343',1,'Initializer::Initializer(const Initializer &amp;x)=delete']]],
  ['initializevectorparams',['initializeVectorParams',['../classInitializer.html#abf9b1455bee83a81468df03c776b186e',1,'Initializer']]],
  ['initializevectorsparams',['initializeVectorsParams',['../classInitializer.html#a18805d6dc50939f731681d8f3a994450',1,'Initializer']]],
  ['initparametersofneuronshodgkinhuxley',['initParametersOfNeuronsHodgkinHuxley',['../initialization_8cpp.html#a3245fcd378a258ae66acc129241668e0',1,'initParametersOfNeuronsHodgkinHuxley(const size_t &amp;nNeurs, const float &amp;VStart, std::vector&lt; std::vector&lt; float &gt; &gt; &amp;initParamsNeurs):&#160;initialization.cpp'],['../initialization_8h.html#a3245fcd378a258ae66acc129241668e0',1,'initParametersOfNeuronsHodgkinHuxley(const size_t &amp;nNeurs, const float &amp;VStart, std::vector&lt; std::vector&lt; float &gt; &gt; &amp;initParamsNeurs):&#160;initialization.cpp']]],
  ['initparametersofneuronsizhikevich',['initParametersOfNeuronsIzhikevich',['../initialization_8cpp.html#a1f5d0844d91029b5e7742e7df45ade76',1,'initParametersOfNeuronsIzhikevich(const size_t &amp;nNeurs, const float &amp;VStart, std::vector&lt; std::vector&lt; float &gt; &gt; &amp;initParamsNeurs):&#160;initialization.cpp'],['../initialization_8h.html#a1f5d0844d91029b5e7742e7df45ade76',1,'initParametersOfNeuronsIzhikevich(const size_t &amp;nNeurs, const float &amp;VStart, std::vector&lt; std::vector&lt; float &gt; &gt; &amp;initParamsNeurs):&#160;initialization.cpp']]],
  ['ip3',['IP3',['../classAstrocyte.html#a6bd7873ece9d4bfc413b8e88295633f7',1,'Astrocyte']]],
  ['isgpudevice',['isGPUDevice',['../main_8cpp.html#a2ac8b266e96d4d003b45f48f57ac03cf',1,'main.cpp']]],
  ['isynpart_5f',['ISynPart_',['../group__Connects.html#gab26f52f5b1fcbd440703843b01e40857',1,'ConnsCPU::ISynPart_()'],['../classConnsGPU.html#a0223e63a3ce38d8d2dd3b47f408d5650',1,'ConnsGPU::ISynPart_()']]],
  ['izhikevichmodel',['IzhikevichModel',['../group__Neurons.html#gga0ee8968d373cea6ea63d5fe6209ccac9a4c41b4777d37cc40c8f19cf649ee740b',1,'types_neuron_models.h']]],
  ['izhikevichneurs',['IzhikevichNeurs',['../classIzhikevichNeurs.html',1,'']]],
  ['izhikevichneurscpu',['IzhikevichNeursCPU',['../classIzhikevichNeursCPU.html',1,'']]],
  ['izhikevichneursgpu',['IzhikevichNeursGPU',['../classIzhikevichNeursGPU.html',1,'']]]
];
