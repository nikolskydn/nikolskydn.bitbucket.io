var searchData=
[
  ['main',['main',['../main_8c.html#a0ddf1224851353fc92bfbff6f499fa97',1,'main.c']]],
  ['make_5frand_5fconnections',['make_rand_connections',['../models__connections_8c.html#ae06014d62f5fa698073539e4bfb65003',1,'make_rand_connections(int _N_neur, int _N_exc_neur, int _N_con, float *_min_weight_con, float *_max_weight_con, int _uniq, int *_pre_con_arr, int *_post_con_arr, float *_weights_con_arr):&#160;models_connections.c'],['../models__connections_8h.html#ae06014d62f5fa698073539e4bfb65003',1,'make_rand_connections(int _N_neur, int _N_exc_neur, int _N_con, float *_min_weight_con, float *_max_weight_con, int _uniq, int *_pre_con_arr, int *_post_con_arr, float *_weights_con_arr):&#160;models_connections.c']]],
  ['make_5frand_5fexternal_5fcurrent',['make_rand_external_current',['../models__neurons_8c.html#a40c64c6d2c9d1fb562cb47689f8ebd34',1,'make_rand_external_current(int _N_neur, float *_I_ext_max, int _prec, int _uniq, float *_I_ext):&#160;models_neurons.c'],['../models__neurons_8h.html#a40c64c6d2c9d1fb562cb47689f8ebd34',1,'make_rand_external_current(int _N_neur, float *_I_ext_max, int _prec, int _uniq, float *_I_ext):&#160;models_neurons.c']]]
];
