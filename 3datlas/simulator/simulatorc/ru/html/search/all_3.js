var searchData=
[
  ['i_5fext',['I_ext',['../create__add__mass_8c.html#a42a20ece44fd743e0ff6d463fb8e46e9',1,'I_ext():&#160;create_add_mass.c'],['../free__add__mass_8c.html#a8615bb795796a244e91d9a31d3d7b625',1,'I_ext():&#160;free_add_mass.c']]],
  ['i_5fext_5fnode',['I_ext_node',['../create__add__mass_8c.html#a52189410b30e140fa5eac9e85024ff26',1,'I_ext_node():&#160;create_add_mass.c'],['../free__add__mass_8c.html#a765cf387cc10dba604f829dfc2b6a59a',1,'I_ext_node():&#160;free_add_mass.c']]],
  ['i_5fsyn',['I_syn',['../create__add__mass_8c.html#af823f661040c0f40911daa0421d4fd47',1,'I_syn():&#160;create_add_mass.c'],['../free__add__mass_8c.html#a5a394a1a05a6aa4b64702d56f60bab7e',1,'I_syn():&#160;free_add_mass.c']]],
  ['i_5fsyn_5fnode',['I_syn_node',['../create__add__mass_8c.html#a40bf63288c0b34800ad6e6c8d6bf9593',1,'I_syn_node():&#160;create_add_mass.c'],['../free__add__mass_8c.html#a25c1f4c596e05c80e22cc6e26dcccee5',1,'I_syn_node():&#160;free_add_mass.c']]],
  ['i_5fsynaptic_5fexp',['I_synaptic_exp',['../models__connections_8c.html#a837d290d0d532701d94a9ef48b0a00a0',1,'I_synaptic_exp(int _N_neur, float *_V_arr, float *_dt, float *_tau, float *_V_lim, int _N_con, float *_y_prev_arr, int *_pre_con_arr, int *_post_con_arr, float *_weights_arr, float *_I_syn_arr, float *_y_curr_arr):&#160;models_connections.c'],['../models__connections_8h.html#a7b081e8e8a8f4212c652a6a4a27e8e39',1,'I_synaptic_exp(int _N_neur, float *_V_arr, float *_dt, float *_tau, float *_V_lim, int _N_con, float *_y_prev_arr, int *_pre_con_arr, int *_post_con_arr, float *_weights_con_arr, float *_I_syn_arr, float *_y_curr_arr):&#160;models_connections.c']]],
  ['if',['if',['../create__add__mass_8c.html#a8291a32f5ca2f25a8d5c5905caf1a701',1,'create_add_mass.c']]]
];
