var searchData=
[
  ['i_5fsynaptic_5fexp',['I_synaptic_exp',['../models__connections_8c.html#a837d290d0d532701d94a9ef48b0a00a0',1,'I_synaptic_exp(int _N_neur, float *_V_arr, float *_dt, float *_tau, float *_V_lim, int _N_con, float *_y_prev_arr, int *_pre_con_arr, int *_post_con_arr, float *_weights_arr, float *_I_syn_arr, float *_y_curr_arr):&#160;models_connections.c'],['../models__connections_8h.html#a7b081e8e8a8f4212c652a6a4a27e8e39',1,'I_synaptic_exp(int _N_neur, float *_V_arr, float *_dt, float *_tau, float *_V_lim, int _N_con, float *_y_prev_arr, int *_pre_con_arr, int *_post_con_arr, float *_weights_con_arr, float *_I_syn_arr, float *_y_curr_arr):&#160;models_connections.c']]],
  ['if',['if',['../create__add__mass_8c.html#a8291a32f5ca2f25a8d5c5905caf1a701',1,'create_add_mass.c']]]
];
