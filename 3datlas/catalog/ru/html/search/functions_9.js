var searchData=
[
  ['operator_20const_20pgresult_20_2a',['operator const PGresult *',['../classSQLResultForReader.html#a532399aabea4ffc9506e82fc7dc9f32e',1,'SQLResultForReader']]],
  ['operator_20pgresult_20_2a',['operator PGresult *',['../classSQLResultForReader.html#ae5e95c2c81bbaa573170b537af94fa56',1,'SQLResultForReader']]],
  ['operator_2a',['operator*',['../classSQLResultForReader.html#a94f29a3e171fc7f0874bc5212f1183f0',1,'SQLResultForReader::operator*()'],['../classSQLResultForReader.html#a7005fe43e67aa4c77aa188ac39c1bfe4',1,'SQLResultForReader::operator*() const ']]],
  ['operator_2d_3e',['operator-&gt;',['../classSQLResultForReader.html#aefd6973d7b676c521d4bc7f4b6d692c2',1,'SQLResultForReader::operator-&gt;()'],['../classSQLResultForReader.html#ad32108d42be8092837c62c5345ef614c',1,'SQLResultForReader::operator-&gt;() const ']]],
  ['operator_3d',['operator=',['../classSQLResultForReader.html#a0e6af030c8304ab4645bf168dbe97cd4',1,'SQLResultForReader::operator=(const SQLResultForReader &amp;)=delete'],['../classSQLResultForReader.html#a9f9fa57ec6fe62d5430c8653de160a9b',1,'SQLResultForReader::operator=(SQLResultForReader &amp;&amp;)=delete']]]
];
