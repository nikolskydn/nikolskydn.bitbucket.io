var searchData=
[
  ['c_5fastrocytehalfsize',['c_astrocyteHalfSize',['../namespaceNNSettings.html#a73439d3eaff482dbe5fea37fee3b3d62',1,'NNSettings']]],
  ['c_5fnaconnectionstep',['c_NAConnectionStep',['../namespaceNNSettings.html#aab54ae2654045f1c29e3449dd623d04c',1,'NNSettings']]],
  ['c_5fneuronheight',['c_neuronHeight',['../namespaceNNSettings.html#a2b99ddc424d14793fd42b3c21c87519c',1,'NNSettings']]],
  ['cacheprojectionmatrix',['cacheProjectionMatrix',['../structTransformerCamPrivate.html#af03624a7b87c2d0488665a84a5ec169f',1,'TransformerCamPrivate']]],
  ['cacheviewmatrix',['cacheViewMatrix',['../structTransformerCamPrivate.html#af0d039aa70ad052637bdc648742e9d17',1,'TransformerCamPrivate']]],
  ['color',['color',['../structNNGraphics_1_1NNMaterial.html#a6eda432d7534b273ee9c74a6956feddb',1,'NNGraphics::NNMaterial']]]
];
